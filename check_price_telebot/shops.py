"""
Classes Item,Shop is in this module.

You can add new shops in this module.
    1. Add subclass from Shop
    2. Add parsing method get_items
    3. Create instance of this subclass
        For example subclass "Mvideo" below
    4. Add shop in config dict "aval_shops"
"""

from time import sleep

from bs4 import BeautifulSoup
from selenium import webdriver


class Item:
    def __init__(self,name: str,
                picture: str = '',
                price: str = '',
                link: str = '') -> None:

        self.name=name
        self.picture=picture
        self.price=price
        self.link=link

    def __str__(self) -> str:
        return f"{self.__class__.__name__}" \
            f"(name={self.name}, " \
            f"picture={self.picture}, " \
            f"price={self.price}, " \
            f"link={self.link})"

class Shop:
    def __init__(self,name: str,main_url: str,catalog: str,query: str):
        self.name = name
        self.main_url = main_url
        self.catalog = catalog
        self.query = query
        self.bs_html_data = ''

    def get_html(self,query_text: str):

        query = self.query + query_text.replace(' ','+')
        url = self.main_url + self.catalog + query

        driver = webdriver.Chrome()
        driver.get(url)
        sleep(2)
        bs_html_data = BeautifulSoup(driver.page_source,'html.parser')
        driver.close()

        self.bs_html_data = bs_html_data


class Mvideo(Shop):
    
    def get_items(self):

        items = []
        links = self.bs_html_data.find_all('a',{'class':'product-picture-link'})
        prices = self.bs_html_data.find_all('span',{'class':'price__main-value'})
        for i,link in enumerate(links):
            items.append(Item(name = link.find('img').get('alt'),
                                picture = 'https:' + link.find('img').get('src'),
                                link = self.main_url + link.get("href")
                                )
                        )

        for i,price in enumerate(prices):
            items[i].price = price.text.strip()
        
        self.bs_html_data = ''

        return items


mvideo = Mvideo(name = 'М-видео',
                main_url = 'https://www.mvideo.ru',
                catalog = '/product-list-page?',
                query = 'q=')



class Wildberries(Shop):
    
    def get_items(self):

        items = []
        cards= self.bs_html_data.find_all('a',{'class':'product-card__main j-card-link'})

        for card in cards:
            try:
                price = card.find('ins',{'class':'lower-price'}).text
            except:
                price = card.find('span',{'class':'price'}).text

            items.append(Item(name = card.find('img').get('alt'),
                                picture = 'https:' + card.find('img').get('src'),
                                link = card.get('href'),
                                price = price.strip()
                                )
                        )
        
        self.bs_html_data = ''

        return items

wildberries = Wildberries(name = 'Wildberries',
                            main_url = 'https://www.wildberries.ru',
                            catalog = '/catalog/0/search.aspx?',
                            query = 'search=')
