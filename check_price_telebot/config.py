import os
from . import shops

TELEGRAM_TOKEN = os.environ['TELEGRAM_TOKEN']
reload_timeout = 2

hello_text = "Я создан для поиска товаров в интернет-магазинах. Пока я могу искать в М-видео и Wildberries. Какой товар ты хотел бы найти?"

info_text = "Приветствую. Для моей активации необходимо ввести команду /start"

wait_message = 'Начинаю поиск. Это может занять некоторое время.'

keyboard_message = "Где будем искать?"

search_end_text = 'Поиск завершен. Для выполнения нового поиска введите /start'

failed_picture ='Изображение недоступно'

empty_result = 'К сожалению по твоему запросу ничего не найдено'

aval_shops = {'М-видео':shops.mvideo,
            'Wildberries':shops.wildberries}

all_shop_button_text = 'Во всех'

keyboard_row_width = 3