from time import sleep

import telebot
from telebot import types

from check_price_telebot import config as cfg

bot = telebot.TeleBot(cfg.TELEGRAM_TOKEN)

def get_items_all(chat_id):
    for shop in cfg.aval_shops.values():
        bot.send_message(chat_id, text=f'Идет поиск в {shop.name}',reply_markup=types.ReplyKeyboardRemove())
        shop.get_html(query_text)
        items = shop.get_items()
        if len(items)==0:
            bot.send_message(chat_id, text=cfg.empty_result,reply_markup=types.ReplyKeyboardRemove())
        for i,item in enumerate(items):
            if i < 2:
                item_text = f"""{item.name}
                {item.price}
                {item.link}
                """
                try:
                    bot.send_photo(chat_id,photo=item.picture,parse_mode='HTML')
                except:
                    bot.send_message(chat_id, text=cfg.failed_picture,reply_markup=types.ReplyKeyboardRemove())
                bot.send_message(chat_id, text=item_text,reply_markup=types.ReplyKeyboardRemove())

def send_keyboard(message, text=cfg.keyboard_message):
    keyboard = types.ReplyKeyboardMarkup(row_width=cfg.keyboard_row_width)
    for shop in cfg.aval_shops:
        keyboard.add(types.KeyboardButton(shop))
    keyboard.add(types.KeyboardButton(cfg.all_shop_button_text))
    global query_text
    query_text=message.text  

    message = bot.send_message(
        message.chat.id,
        text=text, 
        reply_markup=keyboard,
    )

    bot.register_next_step_handler(message, search_item)

def search_item(message):
    bot.send_message(message.chat.id, text=cfg.wait_message,reply_markup=types.ReplyKeyboardRemove())
    if message.text == cfg.all_shop_button_text:
        get_items_all(message.chat.id)
    else:
        cfg.aval_shops[message.text].get_html(query_text)
        items = cfg.aval_shops[message.text].get_items()
        if len(items)==0:
            bot.send_message(message.chat.id, text=cfg.empty_result,reply_markup=types.ReplyKeyboardRemove())
        for i,item in enumerate(items):
            if i < 5:
                item_text = f"""{item.name}
                {item.price}
                {item.link}
                """
                try:
                    bot.send_photo(message.chat.id,photo=item.picture,parse_mode='HTML')
                except:
                    bot.send_message(message.chat.id, text=cfg.failed_picture,reply_markup=types.ReplyKeyboardRemove())
                bot.send_message(message.chat.id, text=item_text,reply_markup=types.ReplyKeyboardRemove())
    bot.send_message(message.chat.id, text=cfg.search_end_text,reply_markup=types.ReplyKeyboardRemove())



@bot.message_handler(commands=['start'])
def hello_message(message):
    bot.send_message(message.chat.id, text=cfg.hello_text,reply_markup=types.ReplyKeyboardRemove())

    bot.register_next_step_handler(message, send_keyboard)
    

@bot.message_handler(content_types=['text'])
def info_message(message):
    bot.send_message(message.chat.id, text=cfg.info_text,reply_markup=types.ReplyKeyboardRemove())
    



if __name__ == '__main__':
     bot.infinity_polling()
